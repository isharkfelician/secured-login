<?php
include 'conn.php'; // Database connection
session_start();

if(isset($_POST['submit']) && isset($_POST['user']) && isset($_POST['pass'])){

// Sanitise username
 $user = $_POST['user'];
 $user = stripslashes($user);

//Sanitise user password
 $pass = $_POST['pass'];
 $pass = stripslashes($pass);
 $hashpass = sha1($pass);

 $_SESSION['username'] = $user;
 $_SESSION['pass'] = $pass;
 $_SESSION['last_time'] = time();
 $_SESSION['message'] = '';

 $total_failed_login = 3;
 $lockout_time       = 2;
 $account_locked     = false;

 //Check failed_login attemts
 $data = $conn->prepare('SELECT last_login, failed_login FROM userlogin WHERE user_name = (:user) LIMIT 1;' );
 $data->bindParam(':user', $user, PDO::PARAM_STR);
 $data->execute();
 $row = $data->fetch();

 if( $data->rowCount() == 1 && ( $row['failed_login'] >= $total_failed_login ) ) {

    date_default_timezone_set('Asia/Colombo');

    $last_login = strtotime($row['last_login']);
    $timeout = $last_login + ($lockout_time * 60);
    $time = date("h:i:sa");
    $timenow = strtotime($time);

    // Check to see if enough time has passed, if it hasn't locked the account
    if($timenow < $timeout){
      $account_locked = true;
      echo "The account is locked due to too many login attempts. Please try again in {$lockout_time} minutes. <br>";
    }

 }

 //Check if the user and password matches
 $data = $conn->prepare('SELECT * FROM userlogin WHERE user_name = (:user) AND pass = (:password) LIMIT 1;');
 $data->bindParam(':user', $user, PDO::PARAM_STR);
 $data->bindParam(':password', $hashpass, PDO::PARAM_STR);
 $data->execute();
 $row = $data->fetch();

//if its a valid login
 if( ( $data->rowCount() == 1 ) && ( $account_locked == false ) ){

   //get the user details
   $failed_login = $row[ 'failed_login' ];
   $last_login   = $row[ 'last_login' ];
   $username = $row['user_name'];
   $pwd = $row['pass'];

   if($user == $username && $hashpass == $pwd){
     //Redirect Browser
     //sleep( rand( 1, 3 ) );
     $_SESSION['message'] ="Welcome $user";
     header('Location:welcome.php');

     //Warn the user
     if($failed_login >= $total_failed_login){
       echo "Someone might of been brute forcing your account.";
       echo "Number of login attempts: {$failed_login}. Last login attempt was at: {$last_login}";
     }

     //Update bad login count
     $data = $conn->prepare('UPDATE userlogin SET failed_login = "0" WHERE user_name = (:user);' );
     $data->bindParam(':user', $user, PDO::PARAM_STR);
     $data->execute();

   }

 } else{
   //Delays the execution time
   sleep( rand( 2, 4 ) );
   echo "Invalid username/password.";
   //echo "The account has been locked because of too many failed logins attempts. Please try again in {$lockout_time} minutes.";

   $data = $conn->prepare('UPDATE userlogin SET failed_login = (failed_login + 1) WHERE user_name = (:user);');
   $data->bindParam(':user', $user, PDO::PARAM_STR);
   $data->execute();
 }

 //Update last login time
 $data = $conn->prepare('UPDATE userlogin SET last_login = now() WHERE user_name = (:user) LIMIT 1;');
 $data->bindParam(':user', $user, PDO::PARAM_STR);
 $data->execute();

}

?>
<!doctype html>
<html>
<body>
<h1>User Login</h1>

<form method='post' action='login.php'>
<input type='text' name='user' id='user' placeholder='Enter Username' required><br/><br/>
<input type='password' name='pass' id='pass' placeholder='Enter Password' required><br/><br/>
<input type='submit' name='submit' value='Submit'><br>
<p>Haven't registered yet ? <a href="index.php">Register here<a/></p>

</form>

</body>
</html>
