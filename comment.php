<?php
include 'conn.php';

//setComment function
function setComment($conn){
  if(isset($_POST['submit_btn'])) {

    $user = $_SESSION['username'];  //get username
    $msg = trim($_POST['comment']); //get comment

    //Sanitise username and comment
    $user = stripslashes($user);
    $msg = stripslashes($msg);
    $msg = htmlspecialchars($msg);
    date_default_timezone_set('Asia/Colombo'); //datetime in sri lanka
    $date = date('Y-m-d H:i:s');

    //update Database
    $data = $conn->prepare('INSERT INTO comments (user, comment, date) VALUES ( :user, :comment, :date);' );
    $data->bindParam(':user', $user, PDO::PARAM_STR);
    $data->bindParam(':comment', $msg, PDO::PARAM_STR);
    $data->bindParam(':date', $date, PDO::PARAM_STR);
    $data->execute();

    header('location: welcome.php');
  }
}

//get comment function
function getComment($conn){

$user = $_SESSION['username'];  //get username

//display all the comments
$data = $conn->prepare('SELECT * FROM comments WHERE user = (:user);');
$data->bindParam(':user', $user, PDO::PARAM_STR);
$data->execute();

while($row = $data->fetch()){
  echo "<div class = 'comment-box'><p>";
  echo $row['user']."<br>";
  echo $row['date']."<br><br>";
  echo nl2br($row['comment']);
  echo "</p></div>";
 }
}

//UploadImage function
function uploadImage($conn){

  if(isset($_POST['upload']) ) {
    //path to store the uploaded image
    $target = "uploads/".basename($_FILES['image']['name']);

    //get all the submitted data from the form
    $image = $_FILES['image']['name'];
    $image_ext  = substr( $image, strrpos( $image, '.' ) + 1);
    $image_size = $_FILES[ 'image' ][ 'size' ];
    $image_tmp  = $_FILES[ 'image' ][ 'tmp_name' ];

  if( ( strtolower( $image_ext ) == "jpg" || strtolower( $image_ext ) == "jpeg" || strtolower( $image_ext ) == "png" ) &&
  		( $image_size < 100000 ) && getimagesize( $image_tmp ) ) {


    $data = $conn->prepare('INSERT INTO images(avatar) VALUES(:image);');
    $data->bindParam(':image', $image, PDO::PARAM_STR);
    $data->execute();

    if(move_uploaded_file($image_tmp, $target)) {
      echo 'Image uploaded succesfully';
    } else {
      echo 'Error occured';
    }

  } else {
		// Invalid file
		echo "Upload failed.";
	}
 }
}

//getImage function
function getImage($conn){
  $data = $conn->prepare('SELECT * FROM images;');
  $data->execute();

  while($row = $data->fetch()) {
    echo "<tr>";
    echo "<td>";?> <img src = "<?php echo $row['avatar'];?>" height = "100" width="100"> <?php echo "</td>";

    echo "</tr>";
  }
}

 ?>
