<?php
session_start();
$_SESSION['message'] = '';

include 'conn.php'; // Database connection

  if ($_SERVER['REQUEST_METHOD'] == 'POST'){

    //$name = $_POST['name'];
    $name = stripslashes($_POST['name']);
    $user = stripslashes($_POST['user']);
    $pw1 = stripslashes($_POST['pass']);
    $pw2 = stripslashes($_POST['pass2']);
    $encryptpw = sha1($pw1);


    if($pw1== $pw2){

      $data = $conn->prepare('SELECT * FROM register WHERE user_name = (:user) LIMIT 1;');
      $data->bindParam(':user', $user, PDO::PARAM_STR);
      $data->execute();
      $num_rows = $data->fetchColumn();

      if($num_rows > 0){

        echo "Choose a different username.";

      }else if(preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=!\?]{8,20}$/', $pw1)){
        $_SESSION['name'] = $name;
        $_SESSION['username'] = $user;
        $_SESSION['pass'] = $encryptpw;
        $_SESSION['last_time'] = time();

        $data = $conn->prepare('INSERT INTO register(name, user_name) VALUES(:name, :user);');
        $data->bindParam(':name', $name, PDO::PARAM_STR);
        $data->bindParam(':user', $user, PDO::PARAM_STR);
        $data->execute();
        /*$sql1 = "INSERT INTO register(name,user_name) VALUES('$name','$user')";
        mysqli_query($conn, $sql1);*/

        $data = $conn->prepare('INSERT INTO userlogin(user_name, pass) VALUES(:user, :pass);');
        $data->bindParam(':user', $user, PDO::PARAM_STR);
        $data->bindParam(':pass', $encryptpw, PDO::PARAM_STR);
        $data->execute();
        /*$sql2 = "INSERT INTO userlogin(user_name,pass) VALUES('$user','$encryptpw')";
        mysqli_query($conn, $sql2);*/

        $_SESSION['message'] ="Welcome $user";
        header("location: welcome.php");

      } else {
        echo "Your password should include at least one Uppercase, Lowercase, Special Character and a digit.";
      }
  }
  else{
    echo "Two passwords does not match.";
  }

}

?>

<html>
<head>
  <title>Register</title>
</head>
<body>
  <h1>Registration Form</h1>

  <form action="index.php" method="POST" >
    <div class="alert alert-error"><?= $_SESSION['message'] ?></div>
    <input type = "text" name ="name" placeholder="Enter Name" required><br/><br/>
    <input type = "text" name = "user" placeholder="Enter username" required><br/><br/>
    <input type = "password" name="pass" placeholder="Enter password" required><br/><br/>
    <input type = "password" name="pass2" placeholder="Confirm password" required><br/><br/>
    <input type="submit" name="submitbtn" value="Register"><br/><br/>
    <p>Already a user ? <a href="login.php">Login here<a/></p>
  </form>
</body>
</html>
